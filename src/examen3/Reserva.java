/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen3;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Reserva {
    
    //ejercicio 1
    //1a
    public void setFechaReserva(Date fecha){
        this.fechaReserva = fecha;
    } 
    
    public Equipo getEquipo1(){
        return equipo1;
    }
    
    public void setEquipo1(Equipo e1){
        this.equipo1 = e1;
    }
    
    public Equipo getEquipo2(){
        return equipo2;
    }
    //ejercicio 3
    public boolean hayJugadoresRepetidos(Equipo e1, Equipo e2){
        for(String p : e1.getJugadores()){
            for(String m : e2.getJugadores()){
                if(p.equals(m)) return true;
            }
        }
        return false;
    }
}
    