/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen3;

import java.util.List;

/**
 *
 * @author Alberto
 */
public class Equipo {
    private List<String> jugadores;
    
    //ejercicio 1
    //1b
    
    private int max_jugadores;
    
    //1c
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getNombre(){
        return nombre;
    }
    public void setMaxJugadores(int max_jugadores){
        this.max_jugadores = max_jugadores;
    }
    public int getMaxJugadores(){
        return max_jugadores;
    }
    
    public boolean addJugador(String jugador){
        if(jugadores.size()<= max_jugadores){
            jugadores.add(jugador);
            return true;
        }
        return false;
    }
    
    public List<String> getJugadores(){
        return jugadores;
    }
}
