/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen7;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Album {

// ejercicio 1
    
    //A
    private List<Foto> fotos;
    private String nombre;
    private int cantidadMaxima;
    
    //B
    private void addFoto(Foto foto){
        fotos.add(foto);
    }
    
    //C
    private Foto getFoto(int posicion){
        return fotos.get(posicion);
    }
    
// ejercicio 2
    public boolean tieneFotoConTamañoMenor(int umbral){
        for(Foto p : fotos) if (p.getTamaño() < umbral) return true; // A traves de este metodo se sabe si un album tiene fotos mal guardadas.
        return false;
    }
    
    // opcion 2 con iterador.
     public boolean tieneFotoConTamañoMenor2(int umbral){
        for (Iterator<Foto> it = fotos.iterator(); it.hasNext();) {
            Foto p = it.next();
            if (p.getTamaño() < umbral) return true; 
        }
        return false;
    }
    // opcion 3 con for tradicional
     
     public boolean tieneFotoConTamañoMenor3(int umbral){
        for(int i = 0; i <= fotos.size(); i++) if (fotos.get(i).getTamaño() < umbral) return true;
        return false;

// ejercicio 3
/*Corresponderia la respuesta 2 pero tiene un problema, cada vez que encuentre una foto menor al umbral que sea mayor a la que se ubiera o ubieran encontrado antes
sobre escribiria la posicion de esta eliminandola de la lista.*/
     
    }
}

