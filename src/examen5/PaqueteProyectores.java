/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen5;

/**
 *
 * @author cecla
 */
public class PaqueteProyectores {
    //ejercicio 1
    //1a
    private int codPaquetesDeProyectores;
    private float costoEnvio;
    private boolean enTransito;

    public int getCodPaquetesDeProyectores() {
        return codPaquetesDeProyectores;
    }

    public void setCodPaquetesDeProyectores(int codPaquetesDeProyectores) {
        this.codPaquetesDeProyectores = codPaquetesDeProyectores;
    }

    public float getCostoEnvio() {
        return costoEnvio;
    }

    public void setCostoEnvio(float costoEnvio) {
        this.costoEnvio = costoEnvio;
    }

    public boolean isEnTransito() {
        return enTransito;
    }

    public void setEnTransito(boolean enTransito) {
        this.enTransito = enTransito;
    }
    
    
}
