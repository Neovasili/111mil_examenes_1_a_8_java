/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen6;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cecla
 */
public class Stock {
    private List<ItemStock> items;

    public List getItems(){ 
        return items;
    }
    public void setItems(List<ItemStock> newItems){
        this.items = newItems;
    }
    
    public List<ItemStock> consultarItemsFaltantes(int cantidadMaxima){
        int i = 0;
        List<ItemStock> resultado = new ArrayList<ItemStock>();
                
        for (ItemStock p : items){ //define un objeto de la clase que contiene el ArrayList y luego recorre el ArrayList items.
            if (p.getCantidad() < cantidadMaxima) resutado.add(p); 
            i++;    
        }
        return resultado;
    }
}
