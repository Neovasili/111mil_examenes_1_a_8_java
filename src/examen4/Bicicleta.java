/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen4;

/**
 *
 * @author cecla
 */
public class Bicicleta {
   
    private String nroDeSerie;
    private float precio;
    
    public Bicicleta (){
        nroDeSerie = "0";
    }
    
    //c
    public void setPrecio(float precio){
        this.precio = precio;
    }

    public float getPrecio(){
        return precio;
    }

    public String getNroDeSerie() {
        return nroDeSerie;
    }

    public void setNroDeSerie(String nroDeSerie) {
        this.nroDeSerie = nroDeSerie;
    }
    
}
