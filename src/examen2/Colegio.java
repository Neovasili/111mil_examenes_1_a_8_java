/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Colegio {
    private List<Curso> cursos;
    
    //ejercicio 2 parte 1 (parte 2 en Curso)
    
    public int Inscriptos(String curso){
        Iterator<Curso> iter = cursos.iterator();
        Curso Cursocomp;
        while(iter.hasNext()){
            Cursocomp = iter.next();
            if (Cursocomp.getNombreCurso().equals(curso))
                    return Cursocomp.cantidadDeInscriptos();
        }
        return -1; //En caso de que no encuentre el nombre del curso
    }    
    
    public int Aprobados(String curso){
        Iterator<Curso> iter = cursos.iterator();
        Curso Cursocomp;
        while(iter.hasNext()){
            Cursocomp = iter.next();
            if (Cursocomp.getNombreCurso().equals(curso))
                    return Cursocomp.cantidadDeAprobados();
        }
        return -1; //En caso de que no encuentre el nombre del curso
    }    
    
    //ejercicio 2 opcion 2
    
    public int Aprobados2(String curso){
                       
        for(Iterator<Curso> iter = cursos.iterator(); iter.hasNext();){
            Curso Cursocomp = iter.next(); // por cada vez que recorre lo crea y lo inicia?
            if (Cursocomp.getNombreCurso().equals(curso)){
                List<Inscripcion> listadoinscriptos = Cursocomp.getInscriptos();
                int contador = 0;
                for(Iterator<Inscripcion> iter2 = listadoinscriptos.iterator(); iter2.hasNext();){
                    Inscripcion Inscrip = iter2.next();
                    if(Inscrip.getNota()< Cursocomp.getNotaAprobacion())
                        contador ++;
                }
                return contador;
            } 
        }
        return -1; //En caso de que no encuentre el nombre del curso
    }    

    // ejercicio 2 opcion3
    
    public int Aprobados3(String curso){
        List<Inscripcion> listadoinscriptos = new ArrayList<Inscripcion>();
        Iterator<Curso> iter = cursos.iterator();
        Curso Cursocomp;
        
        while(iter.hasNext()){
            Cursocomp = iter.next();
            if (Cursocomp.getNombreCurso().equals(curso)){
                listadoinscriptos = Cursocomp.getInscriptos();
                Iterator<Inscripcion> iter2 = listadoinscriptos.iterator();
                Inscripcion Inscrip;
                int contador = 0;
                while(iter2.hasNext()){
                    Inscrip = iter2.next();
                    if(Inscrip.getNota()< Cursocomp.getNotaAprobacion())
                        contador ++;
                }
                return contador;
            } 
        }
        return -1; //En caso de que no encuentre el nombre del curso
    }
    
}
