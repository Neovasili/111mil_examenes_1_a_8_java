/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

/**
 *
 * @author Alberto
 */
public class Alumno {
    //b
    private int dni;
    private String nombre;
    private String apellido;
    private int edad;
    
    public int getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    
    public boolean equals(Alumno a){
        boolean condicion;
        condicion = true;
        
        condicion = (this.dni == a.getDni()) && condicion;
        condicion = (this.nombre == a.getNombre()) && condicion;
        condicion = (this.apellido == a.getApellido()) && condicion;
        condicion = (this.edad == a.getEdad()) && condicion;
        return condicion;
    }
    
}
