/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Curso {
    private List<Inscripcion> inscripciones;

    //a1
    private String nombreCurso;
    private int cupo;
    private float notaAprobacion;
    
    public void setNombreCurso(String n){
        nombreCurso = n;
    }
    
    public String getNombreCurso(){
        return nombreCurso;
    }
    
    //ejercicio 2 parte 2 (parte 1 en Colegio)
    
    public int cantidadDeInscriptos(){
        return inscripciones.size();
    }
    
    public int cantidadDeAprobados(){
        Inscripcion inscrip;
        int aprobados = 0;
        Iterator<Inscripcion> it = inscripciones.iterator();
        while (it.hasNext()) {
            inscrip = it.next();
            if (inscrip.getNota() >= notaAprobacion) aprobados++;
        }
    return aprobados;
    }

    //ejercicio 2 opcion 2
    
    public List getInscriptos(){
        return inscripciones;
    }

    public float getNotaAprobacion() {
        return notaAprobacion;
    }

}
